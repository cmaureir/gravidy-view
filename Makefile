CXXFLAGS = -Wall  \
           -lglut \
           -lGLU \
           -lGL  \
           -lstdc++  \
           -pedantic \
           -DGL_GLEXT_PROTOTYPES \
           -lm \
           -lboost_program_options

all: gravidy-view

Bitmap.o: include/Bitmap.cpp include/Bitmap.h
core.o: include/core.cpp include/core.hpp
gravidy-view: include/Bitmap.o include/core.o

clean:
	@rm -vf include/*.o gravidy-view

.PHONY: all clean
