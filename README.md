# GraviDyView

## N-body 3D visualization

 * **Author:** Cristián Maureira-Fredes
 * **Date:** 03/19/2012

### Dependencies

 * ``boost``, for the options parser.
 * ``GLut``, to use OpenGL.

### Installation

    $ make

### Execution

    $ ./gravidy-view -i input_files

### Input structure

We based this work on `GraviDy`, so you can easily use one or more snapshots from
the code, but if you want to visualize different output files,
the minimum structure requierement is::

    id0 rx0 ry0 rz0
    id1 rx1 ry1 rz1
    ...
    idN rxN ryN rzN

where N is the amount of particles.
Extra columns besides `id` `rx` `ry` `rz` are ignored.

### Screenshot

![alt text](screenshot.png "Screnshot 1")


### Comments

The files `Bitmap.{h,cpp}` was downloaded from http://gamedev.net developed by Mark Bernard.

 * Provide the functionality to load a BMP file into a C++ array.
 * This is necesary to load the partcles textures.
