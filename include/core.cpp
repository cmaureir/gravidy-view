/*
 * Copyright (c) 2012
 *
 * Cristián Maureira-Fredes <cmaureirafredes@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. The name of the author may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "core.hpp"

// Variable declaration
GLsizei sww, swh, swz;
GLdouble wleft, wright, wbottom, wtop, wnear, wfar;
GLfloat sizes[2];
GLuint g_textureID;

vector<body> bodies;
vector<body> positions;

int n, it, j, r;
int alpha;
int window_id;
float quadratic[3];
float angle_theta, angle_phi;
float eye_x, eye_y, eye_z;
float dir_x, dir_y, dir_z;
float up_x, up_y, up_z;
float zoom;
int GLOBAL = 0;
vector<string> input_file;
int xangle, yangle, zangle;

// Variable initialization
void init_var()
{
    sww = swh = swz = 1000;
    xangle = yangle = zangle = 0;

    wleft   = -(GLdouble)sww*2; wright = (GLdouble)sww*2;
    wbottom = -(GLdouble)swh*2; wtop   = (GLdouble)swh*2;
    wnear   = -(GLdouble)swz*2; wfar   = (GLdouble)swz*2;

    alpha = 100;
    zoom = -500.0;
    j = 0;
    r = 10;

    quadratic[0] = 0.01f;
    quadratic[1] = 0.01f;
    quadratic[2] = 0.01f;

    angle_theta = 0.0;
    angle_phi = 0.0;

    eye_x = 0.0; eye_y = 0.0; eye_z = 5.0;
    dir_x = 0.0; dir_y = 0.0; dir_z = 0.0;
    up_x  = 0.0; up_y  = 1.0; up_z  = 0.0;
}

// OpenGL texture initialization
void init(void)
{
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glEnable(GL_DEPTH_TEST);

    // Loading BMP
    Bitmap *image = new Bitmap();
    //string texture_file = "texture/particle.bmp";
    string texture_file = "texture/star.bmp";
    if (image->loadBMP(texture_file.c_str()) == false)
    {
      cerr << "Error opening bmp" <<endl;
      exit(0);
    }

    // Enabling 2D texture
    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &g_textureID);

    glBindTexture(GL_TEXTURE_2D, g_textureID);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Loading BMP to texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->data);

}

/*
 * Display the bodies using the texture
 */
void DisplayBodies()
{
    glMatrixMode(GL_MODELVIEW);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glTranslatef(0,0,0);

    glLoadIdentity();
    // Updating camera position
    gluLookAt(eye_x, eye_y, eye_z, dir_x,dir_y, dir_z, up_x, up_y, up_z);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-2 + zoom,
             2 - zoom,
            -2 + zoom,
             2 - zoom,
            -2 + zoom,
             2 - zoom); // Changed some of the signs here

    //glOrtho(0, sww, swh, 0, 0, 1);

    // Rotation in every axis
    glRotatef(xangle, 1, 0, 0 );
    glRotatef(yangle, 0, 1, 0 );
    glRotatef(zangle, 0, 0, 1 );

    // Enabling and configuring the BLEND
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_CONSTANT_COLOR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glDepthMask(GL_FALSE);
    glGetFloatv(GL_ALIASED_POINT_SIZE_RANGE, sizes);

    // Enabling and configuring POINT_SPRITE
    glEnable(GL_POINT_SPRITE_ARB);

    glPointParameterfARB(GL_POINT_SIZE_MAX_ARB, sizes[1] + 0.0f);
    glPointParameterfARB(GL_POINT_SIZE_MIN_ARB, sizes[0] + 5.0f);
    // Perspective distance, some particles will be showed close to the camera
    glPointParameterfvARB(GL_POINT_DISTANCE_ATTENUATION_ARB, quadratic);

    glTexEnvi(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE);

    glDisable(GL_TEXTURE_2D);
    glLineWidth(1.5f);
    glBegin(GL_LINES);
        glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
        glVertex3f(0, 0, 0);
        glVertex3f(500, 0, 0);

        glColor4f(0.0f, 1.0f, 0.0f, 0.5f);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 500, 0);


        glColor4f(0.0f, 0.0f, 1.0f, 0.5f);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0, 500);
    glEnd();
    glEnable(GL_TEXTURE_2D);

    // White bodies with alpha channel
    glColor4f (1.0f,1.0f,1.0f,0.8f);
    //glColor4f (1.0f,1.0f,1.0f,1.0f);

    //glColor4f (1.0f,1.0f,1.0f,1.0f);
    glBindTexture(GL_TEXTURE_2D, g_textureID);

    // Draw points
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable(GL_POINT_SMOOTH);
    glPointSize(8.0f);
    glBegin(GL_POINTS);
        for(int i = 0; i < n; i++)
        {
            glVertex3f( bodies[i].x*alpha,
                        bodies[i].y*alpha,
                        bodies[i].z*alpha);
        }
    glEnd();
    std::cout << "Zoom: " << zoom << std::endl;


    usleep(120000);
    //sleep(1);
    GLOBAL++;
    //usleep(1000000);

    // Disabling environment
    glDisable(GL_POINT_SPRITE_ARB);
    glDisable(GL_BLEND);

    // References lines of the axis
    //glClearColor(1, 1, 1, 1);

    //if(it/n <= GLOBAL)
    //    getchar();
}

// Window refresh function
void refresh(void)
{
    glutSetWindow(window_id);

    // Bodies position update
    if (j < it - 1)
    {
        for (int  i=0; i < n; i++)
        {
            bodies[i].x = positions[j].x;
            bodies[i].y = positions[j].y;
            bodies[i].z = positions[j].z;
            j++;
        }
    }

    DisplayBodies();
    glFlush();
}

// Reshape function
void reshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(wleft, wright, wbottom, wtop, wnear, wfar);
}

// Keys functionality
void keys(unsigned char key, int x, int y)
{
    switch(key)
    {
        case 'z':
            zoom += 100; break;
        case 'Z':
            zoom -= 100; break;
        case 'j':
            yangle += 5;break;
        case 'k':
            yangle -= 5;break;
        case 'l':
            xangle += 5;break;
        case 'h':
            xangle -= 5;break;
        case 'i':
            zangle += 5;break;
        case 'u':
            zangle -= 5;break;
        case 27:
            exit(0);
            break;
    }
}

// Mouse functionality
void mouse(int button, int state, int x, int y)
{
    switch(button)
    {
        case 3:
            zoom += 100; break;
        case 4:
            zoom -= 100; break;
    }

}

// Mouse motion functionality
void mouse_motion(int x, int y)
{
   // Mouse point to angle conversion
   angle_theta = (360.0/swh)*y*3.0;    //3.0 rotations possible
   angle_phi   = (360.0/sww)*x*3.0;

   // Restrict the angles within 0~360 deg (optional)
   if(angle_theta > 360) angle_theta = fmod((double)angle_theta,360.0);
   if(angle_phi   > 360) angle_phi   = fmod((double)angle_phi,360.0);

   // Spherical to Cartesian conversion.
   // Degrees to radians conversion factor 0.0174532
   eye_x = r * sin(angle_theta*0.0174532) * sin(angle_phi*0.0174532);
   eye_y = r * cos(angle_theta*0.0174532);
   eye_z = r * sin(angle_theta*0.0174532) * cos(angle_phi*0.0174532);

   // Reduce angle_theta slightly to obtain another point on the same longitude line on the sphere.
   GLfloat dt = 1.0;
   GLfloat eye_xtemp = r * sin(angle_theta*0.0174532-dt) * sin(angle_phi*0.0174532);
   GLfloat eye_ytemp = r * cos(angle_theta*0.0174532-dt);
   GLfloat eye_ztemp = r * sin(angle_theta*0.0174532-dt) * cos(angle_phi*0.0174532);

   // Connect these two points to obtain the camera's up vector.
   up_x=eye_xtemp-eye_x;
   up_y=eye_ytemp-eye_y;
   up_z=eye_ztemp-eye_z;

   glutPostRedisplay();
}

bool check_options(int argc, char *argv[])
{
    po::options_description desc("Options");
    desc.add_options()
        ("help,h"   , "Display message")
        ("input,i"  , po::value<std::vector<std::string>>()->multitoken(), "Input data filename")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    // Help option
    if (vm.count("help"))
    {
        std::cerr << desc
                  << std::endl;
        return false;
    }

    // Input option
    if (vm.count("input"))
    {
        input_file = vm["input"].as< std::vector<std::string> >();

        for(std::vector<string>::iterator it = input_file.begin();
            it != input_file.end();
            ++it)
        {
            if(!file_exists(*it))
            {
                std::cout << "gravidy: cannot access "
                          << *it
                          << ": No such file or directory"
                          << std::endl;
                return false;
            }
        }
        read_input_file(input_file);
    }
    else
    {
        std::cerr << "gravidy: option requires an argument -- 'input'"
                  << std::endl;
        std::cerr << desc
                  << std::endl;
        return false;
    }

    return true;
}

void read_input_file(std::vector<std::string> files)
{

    std::cout <<" Number of files: " << files.size() << std::endl;
    std::vector<string>::iterator ff;

    for(ff = files.begin(); ff != files.end(); ++ff)
    {
        //std::cout << "File: " << (*ff) << std::endl;

        std::ifstream file((*ff).c_str());

        body part;
        std::istream_iterator<std::string> word_iter(file), word_iter_end;

        word_iter++; // Skipping "#"
        //std::cout << "First: " << *word_iter << std::endl;
        word_iter++; // Skipping "\n"

        for( /* empty */ ; word_iter != word_iter_end; ++word_iter)
        {
            part.id = strtod((*word_iter).c_str(), NULL);
            word_iter++;

            word_iter++; // Skipping "Mass"

            part.x = strtod((*word_iter).c_str(), NULL);
            word_iter++;

            part.y = strtod((*word_iter).c_str(), NULL);
            word_iter++;

            part.z = strtod((*word_iter).c_str(), NULL);

            word_iter++; // Skipping "vx"
            word_iter++; // Skipping "vy"
            word_iter++; // Skipping "vz"

            //std::cout << part.id << " | " << part.x << " " << part.y << " " << part.z <<std::endl;
            positions.push_back(part);
        }
        it++;

        file.close();
    }
    n =  ((int)positions.size())/it;
    it = n * it;
    bodies.resize(n);

    //std::cout << "Bodies: " << n << std::endl;
    //std::cout << "Iterations: " << it << std::endl;

}

bool file_exists(const std::string& filename)
{
    struct stat buffer;
    if (stat(filename.c_str(), &buffer) != -1)
    {
        return true;
    }
    return false;
}
