/*
 * Copyright (c) 2012
 *
 * Cristián Maureira-Fredes <cmaureirafredes@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. The name of the author may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef COMMON_HPP
#define COMMON_HPP

// OpenGL
#include <GL/glut.h>
#include <GL/glext.h>

// Boost
#include <boost/program_options.hpp>

// C/C++
#include <sys/stat.h>
#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <unistd.h>

// Local
#include "Bitmap.hpp"

namespace po = boost::program_options;

struct body {
    int id;
    double x,y,z;
};

// GL Variables
extern GLsizei sww, swh, swz;   // window sizes
extern GLdouble wleft, wright, wbottom, wtop, wnear, wfar; // Orthographic matrix parameters
extern GLfloat sizes[2];   // Point ARB size
extern GLuint g_textureID; // Texture id

// Vector variables
extern std::vector<body> bodies;    // bodies vector
extern std::vector<body> positions; // bodies new positions vector

// Variables
extern int n, it, j, r;
extern int alpha; // Maximization parameter.
extern int window_id;
extern float quadratic[3]; // Point distance attenuation
extern float angle_theta, angle_phi; // Mouse motion camera angles
extern float eye_x, eye_y, eye_z;    // LookAt Eye coordinates
extern float dir_x, dir_y, dir_z;    // LookAt Dir coordinates
extern float up_x, up_y, up_z;       // LookAt Up  coordinates
extern float zoom;
#endif
