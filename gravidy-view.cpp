/*
 * Copyright (c) 2012
 *
 * Cristián Maureira-Fredes <cmaureirafredes@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. The name of the author may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "include/core.hpp"

int main(int argc, char** argv)
{
    init_var();   // Variable initialization

    if(!check_options(argc,argv)) return 1;

    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_DEPTH); // Single buffer
    glutInitWindowSize(sww, swh);                  // Window size
    glutInitWindowPosition (0, 0);                 // Window position

    window_id = glutCreateWindow("GraviDy View");  // Window title
    init();                                        // Some initialization

    glutDisplayFunc(refresh);           // Display function to draw the particles
    glutReshapeFunc(reshape);           // Particle position update
    glutKeyboardFunc(keys);             // Key functions
    glutMouseFunc(mouse);               // Mouse functions
    glutMotionFunc(mouse_motion);       // Mouse active motion function
    glutIdleFunc(refresh);              // Re-display particles
    glutMainLoop();                     // Glut loop
}
